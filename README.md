# Eigenwerte von quadratischen Matrizen
## Team
Laura Riegert & Stephanie Schmidt

## Funktionsbeschreibung
Folgende Verfahren zur Berechnung der Eigenwerte:
- Charakteristisches Polynom
- Potenzmethode nach Von Mises
- QR-Verfahren(?)
- Mathematica zur Überprüfung
- Visualisierung

## Bezug zur Mathematik
Numerische und analytische Verfahren zur Berechnung der Eigenwerte von Matrizen

## Zeitplan
Grundkonzept bis Ende Oktober

Feinkonzept bis Mitte November

Prototyp bis Ende Dezember

## Aufgabenteilung
Teamarbeit

## Zusammenarbeit
Regelmäßiges Treffen jeden Dienstag

Git

## Dokumentation
Basierend auf Javadoc

Sprache Deutsch

## Offene Punkte, Fragen
100x100 Matrix

QR- Verfahren

Plotter mit Mathematica und Java